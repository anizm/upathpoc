package com.example.demo.business;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.demo.data.ItemRepository;
import com.example.demo.model.Item;

@RunWith(MockitoJUnitRunner.class)
public class ItemBusinessServiceTest {
	
	@InjectMocks	
	ItemBusinessService itemBusinessService = new ItemBusinessService();
	@Mock
	private ItemRepository repository;


	@Test
	public void calculateSumUsingDataService_stub_test() {
		
	    when(repository.findAll()).thenReturn(Arrays.asList(new Item(1,"book",100,100),new Item(2,"ball",200,200)));
	    List<Item> items = itemBusinessService.retrieveAllItems();
	    
		assertEquals(10000, items.get(0).getValue());
		assertEquals(40000, items.get(1).getValue());
	}
     

}

