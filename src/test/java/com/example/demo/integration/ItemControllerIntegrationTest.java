package com.example.demo.integration;


import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)   // launches entire spring boot application context, random port is for continuous integration
public class ItemControllerIntegrationTest {
 
	@Autowired
	private TestRestTemplate restTemplate;
	@Test
	public void contextLoads_test() throws JSONException {
		String response = restTemplate.getForObject("/all-items-from-database", String.class);
		JSONAssert.assertEquals("[{id:1,name:'book'},{id:2},{id:3}]", response, false);
	}

}