package com.example.demo.controller;

import static org.junit.Assert.*;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.demo.business.ItemBusinessService;
import com.example.demo.model.Item;

@RunWith(SpringRunner.class) // launch spring context for unit test
@WebMvcTest(value = ItemController.class)  //(spring mock mvc) only want to launch specific controller 
public class ItemControllerTest {
	
	@Autowired
	private MockMvc mockMvc;  // to call uri, use mockmvc framework
	
	@MockBean // by default we are launching only ItemController class, for the availability of dependency use mockbean
   	ItemBusinessService itemBusinessService;


	@Test
	public void retrieveAllItems_test() throws Exception {
		
		when(itemBusinessService.retrieveAllItems()).thenReturn(Arrays.asList(new Item(2,"Item2",10,10),new Item(3,"Item3",20,20)));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/all-items-from-database").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder)
				.andExpect(status().isOk())
				.andExpect(content().json("\r\n" + 
						"[{\"id\":2,\"name\":\"Item2\",\"price\":10,\"quantity\":10},\r\n" + 
						"{\"id\":3,\"name\":\"Item3\",\"price\":20,\"quantity\":20}]"))
				.andReturn();
	}
}

