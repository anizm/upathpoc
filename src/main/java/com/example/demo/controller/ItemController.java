package com.example.demo.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.business.ItemBusinessService;
import com.example.demo.model.Item;

@RestController
public class ItemController {

@Autowired
ItemBusinessService itemBusinessService;
   @GetMapping("/all-items-from-database")
   public List<Item> retrieveAllItems(){
	   return itemBusinessService.retrieveAllItems();
   }
}
